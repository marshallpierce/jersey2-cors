[![Build Status](https://semaphoreapp.com/api/v1/projects/783207ec-e3a2-4e6a-9bbe-57f6f1e83367/358567/badge.png)](https://semaphoreapp.com/marshallpierce/jersey2-cors)

To learn more about CORS, see documentation from [MDN](https://developer.mozilla.org/en-US/docs/HTTP/Access_control_CORS) and [W3](http://www.w3.org/TR/cors/).
