package org.mpierce.jersey2.cors

object CorsSimpleMediaTypes {
    /**
     * Permissible media types for a simple cross-origin POST
     */
    val SIMPLE_MEDIA_TYPES = listOf("application/x-www-form-urlencoded", "multipart/form-data", "text/plain")
}
