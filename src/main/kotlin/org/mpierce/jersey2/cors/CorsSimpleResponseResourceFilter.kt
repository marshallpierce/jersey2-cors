package org.mpierce.jersey2.cors

import javax.annotation.concurrent.ThreadSafe
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter

@ThreadSafe
public class CorsSimpleResponseResourceFilter(
        private val policy: SimpleRequestPolicy) : ContainerResponseFilter {

    override fun filter(requestContext: ContainerRequestContext, responseContext: ContainerResponseContext) {
        val incomingOrigin = requestContext.getHeaderString(CorsHeaders.ORIGIN)
        if (incomingOrigin == null) {
            return
        }

        // TODO check for simple post media types

        val allowOrigin = policy.getAllowOrigin(requestContext)
        val allowCredentials = policy.allowCredentials(requestContext)

        if (allowCredentials && allowOrigin.equals("*")) {
            // TODO tests
            throw IllegalStateException("Cannot have allowCredentials = true and allowOrigin = *")
        }

        val h = responseContext.getHeaders()

        putIfNotPresent(h, CorsHeaders.ALLOW_ORIGIN, allowOrigin)

        val exposeHeaders = policy.exposeHeaders(requestContext)
        if (!exposeHeaders.isEmpty()) {
            putIfNotPresent(h, CorsHeaders.EXPOSE_HEADERS, exposeHeaders)
        }

        if (allowCredentials) {
            putIfNotPresent(h, CorsHeaders.ALLOW_CREDENTIALS, "true")
        }
    }
}

