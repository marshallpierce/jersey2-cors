package org.mpierce.jersey2.cors.feature

import javax.ws.rs.container.DynamicFeature
import javax.ws.rs.container.ResourceInfo
import javax.ws.rs.core.FeatureContext
import javax.ws.rs.POST
import javax.ws.rs.GET
import javax.ws.rs.HEAD
import org.mpierce.jersey2.cors.CorsSimpleResponseResourceFilter
import org.mpierce.jersey2.cors.SimpleRequestPolicy

/**
 * Applies a CorsSimpleResponseResourceFilter to all POST, GET, and HEAD resource methods with the provided policy.
 */
class ApplySimpleCorsResponseHeadersGloballyFeature(policy: SimpleRequestPolicy) : DynamicFeature {
    private val filter = CorsSimpleResponseResourceFilter(policy)

    override fun configure(resourceInfo: ResourceInfo, context: FeatureContext) {
        val method = resourceInfo.getResourceMethod()
        if (method.getAnnotation(POST::class.java) != null
                || method.getAnnotation(GET::class.java) != null
                || method.getAnnotation(HEAD::class.java) != null) {
            context.register(filter)
        }
    }

}
