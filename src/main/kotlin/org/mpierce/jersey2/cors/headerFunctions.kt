package org.mpierce.jersey2.cors

import javax.ws.rs.core.MultivaluedMap

/**
 * @return true if the header was added
 */
fun putIfNotPresent(h: MultivaluedMap<String, in String>, header: String, value: String): Boolean {
    if (!h.containsKey(header)) {
        h.putSingle(header, value)
        return true
    }

    return false
}
