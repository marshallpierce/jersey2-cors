package org.mpierce.jersey2.cors

import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

@Path("resource")
class BasicResource {
    @GET
    fun get(): String {
        return "get"
    }

    @POST
    fun post(): String {
        return "post"
    }

    @DELETE
    fun delete(): String {
        return "delete"
    }
}
