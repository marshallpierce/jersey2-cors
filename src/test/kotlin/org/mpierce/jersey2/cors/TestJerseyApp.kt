package org.mpierce.jersey2.cors

import org.glassfish.jersey.server.ResourceConfig

class TestJerseyApp(objects: List<Any>, classes: List<Class<*>>) : ResourceConfig() {
    init {
        for (o in objects) {
            register(o)
        }

        for (c in classes) {
            register(c)
        }
    }
}
