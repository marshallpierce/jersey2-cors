package org.mpierce.jersey2.cors.feature

import com.ning.http.client.AsyncHttpClient
import com.ning.http.client.Response
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.glassfish.jersey.servlet.ServletContainer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mpierce.jersey2.cors.BasicResource
import org.mpierce.jersey2.cors.CorsHeaders
import org.mpierce.jersey2.cors.FixedSimpleRequestPolicy
import org.mpierce.jersey2.cors.TestJerseyApp
import javax.ws.rs.core.Response.Status

class ApplySimpleCorsResponseHeadersGloballyFeatureTest {

    var port: Int = -1

    var server = Server()

    var client = AsyncHttpClient()

    @Before
    fun setUp() {
        var servletHolder = ServletHolder(ServletContainer(
                TestJerseyApp(listOf(
                        ApplySimpleCorsResponseHeadersGloballyFeature(
                                FixedSimpleRequestPolicy("*", emptyList(), false))),
                        listOf(BasicResource::class.java))))
        var handler = ServletContextHandler()
        handler.addServlet(servletHolder, "/*")

        server.handler = handler
        val connector = ServerConnector(server)
        server.addConnector(connector)
        server.start()

        port = connector.localPort
    }

    @After
    fun tearDown() {
        server.stop()
        client.close()
    }

    @Test
    fun testAppliesToGet() {
        val response = get("/resource")
        assertEquals(Status.OK.statusCode, response.statusCode)
        assertEquals("*", response.getHeader(CorsHeaders.ALLOW_ORIGIN))
    }

    protected fun get(path: String): Response {
        val boundRequestBuilder = client.prepareGet("http://localhost:" + port + path)
        boundRequestBuilder.addHeader(CorsHeaders.ORIGIN, "http://localhost")
        return boundRequestBuilder.execute().get()!!
    }
}
